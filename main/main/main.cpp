// main.cpp : Defines the entry point for the console application.
//

//
//baza danych z informacjami: Imie, Nazwisko, Stan konta, Nr_karty, PIN (niektorzy moga miec wiele kart) 
//klasa karta,
//wyplata/wplata gotowki, zmiana PIN u/ sprawdzenie stanu konta.

#include <string>
#include <iostream>
#include <vector>
#include <conio.h>
#include "stdafx.h"
#include "Bankomat.h"
#include "Baza.h"

using namespace std;


int main()
{
	vector < Bankomat > osoba;
	cout << "Baza Osob\n1.-Lista osob\n2.-Dodaj osobe\n0.-Wyjscie"<<endl;
	
	int odp;
	do
	{cout << "\nWybierz: ";
		cin >> odp;
		switch (odp)
		{
		case 1:
			cout << "\n\nLista osob:\n";
			for (int i = 0; i < osoba.size(); i++)
			{
				cout << endl;
				cout << "Nazwa: " << osoba[i].imie << endl;
				cout << "Autor: " << osoba[i].nazwisko << endl;
				cout << "Numer konta: " << osoba[i].nr_konta << endl;
				cout << "Pin: " << osoba[i].pin << endl;
			}
			break;
		case 2:
			cout << "\n\n\n|----DODAWANIE_OSOBY_DO_LISTY-----|";
			cout << "\nPodaj imie: ";
			string odp_imie;
			cin >> odp_imie;
			cout << "Nazwisko: ";
			string odp_nazwisko;
			cin >> odp_nazwisko;
			cout << "Numer konta: ";
			string odp_nr_konta;
			cin >> odp_nr_konta;
			cout << "Pin: ";
			string odp_pin;
			cin >> odp_pin;
			osoba.push_back(Bankomat(odp_imie, odp_nazwisko, odp_nr_konta, odp_pin));
			cout << "\n<***ZAKONCZONO_Z_SUKCESEM***>";
			break;
		}
	} while (odp != 0);
	
	return 0;
}

