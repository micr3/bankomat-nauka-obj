#pragma once
#include <vector>
#include "Bankomat.h"

class Baza: private Bankomat
{
private:
	Baza() {}
	vector<Bankomat>baza;
public:
	
	void add(Bankomat b);
	void save();
	void load();

};